package br.puc.poo.exercicio2.modelos;

import java.util.Objects;

/**
 *
 * @author jose-almeida
 */
public class Poligno {

    private Ponto primeiro;
    private Ponto ultimo;

    public Ponto getPrimeiro() {
        return this.primeiro;
    }

    public Ponto getUltimo() {
        return this.ultimo;
    }

    public void setPrimeiro(Ponto primeiro) {
        if (primeiro.equals(this.getUltimo())) {
            throw new IllegalArgumentException();
        }

        this.primeiro = primeiro;
    }

    public void setUltimo(Ponto ultimo) {
        if (ultimo.equals(this.getPrimeiro())) {
            throw new IllegalArgumentException();
        }

        this.ultimo = ultimo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.getPrimeiro());
        hash = 79 * hash + Objects.hashCode(this.getUltimo());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final Poligno other = (Poligno) obj;
        if (!Objects.equals(this.getPrimeiro(), other.getPrimeiro())) {
            return false;
        }

        return Objects.equals(this.getUltimo(), other.getUltimo());
    }

}
