package br.puc.poo.exercicio2.modelos;

/**
 *
 * @author jose-almeida
 */
public class Ponto {

    private int x;
    private int y;
    private Ponto proximo;

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Ponto getProximo() {
        return proximo;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setProximo(Ponto proximo) {
        if (this.equals(proximo)) {
            throw new IllegalArgumentException();
        }

        this.proximo = proximo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.getX();
        hash = 71 * hash + this.getY();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final Ponto other = (Ponto) obj;
        if (this.getX() != other.getX()) {
            return false;
        }

        return this.getY() == other.getY();
    }

}
